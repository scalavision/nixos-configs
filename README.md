nixos-config
============
Kevin Crook <kevinwaynecrook@gmail.com>

Configuration files for my [NixOS](https://nixos.org/) machines.
