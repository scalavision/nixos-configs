{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./roles/grub.nix
    ./roles/common.nix
    ./roles/kodi.nix
  ];

  boot = {
    supportedFilesystems = [ "zfs" ];
    loader.grub.zfsSupport = true;
  };

  services.zfs.trim.enable = true;

  networking = {
    hostName = "zinc";
    hostId = "420A0620";
    firewall.enable = false;
    useDHCP = false;
    bridges.br0.interfaces = [ "enp2s0" ];
    interfaces.br0.useDHCP = true;
  };

  services.xserver = {
    enable = true;
    desktopManager.kodi.enable = true;
    displayManager.autoLogin.enable = true;
    displayManager.autoLogin.user = "kodi";
  };

  users.users.kodi = {
    isNormalUser = true;
    description = "Kodi";
  };

  services.ntp.enable = true;

  system.stateVersion = "20.03";

}

