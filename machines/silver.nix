{ config, lib, pkgs, ... }:

let
  unstableTarball =
    fetchTarball
      https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz;

in

{
  imports = [
    ./hardware-configuration.nix
    ./roles/grub.nix
    ./roles/common.nix
    ./roles/desktop.nix
    ./roles/games.nix
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    unstable = import unstableTarball {
      config = config.nixpkgs.config;
    };
  };

  systemd.services.zenstates = {
    enable = true;
    description = "Zenstates script";
    after = [ "multi-user.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      Type = "oneshot";
      User = "root";
      ExecStart = "${pkgs.unstable.zenstates}/bin/zenstates --c6-disable";
#      ExecStop = "${pkgs.procps}/bin/pkill zenstates";
    };
  };

  systemd.user.services.scream-ivshmem = {
    enable = true;
    description = "Scream IVSHMEM";
    serviceConfig = {
      ExecStart = "${pkgs.scream-receivers}/bin/scream-ivshmem-pulse /dev/shm/scream";
      Restart = "always";
    };
    wantedBy = [ "multi-user.target" ];
    requires = [ "pulseaudio.service" ];
  };

  boot = {
    kernelParams = [ "amd_iommu=on" "pcie_aspm=off" "vga=current" "quiet" "splash" "loglevel=3" "rd.udev.log_priority=3" "vt.global_cursor_default=0" ];
    initrd.availableKernelModules = [ "amdgpu" "vfio-pci" ];
    initrd.preDeviceCommands = ''
      DEVS="0000:0d:00.0 0000:0d:00.1"
      for DEV in $DEVS; do
        echo "vfio-pci" > /sys/bus/pci/devices/$DEV/driver_override
      done
      modprobe -i vfio-pci
    '';
    initrd.secrets = {
      "/silver.key" = "/silver.key";
    };
    kernelModules = [ "msr" "kvm-amd" ];
    kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = [ "zfs" ];
    loader.efi.canTouchEfiVariables = true;
    loader.grub.zfsSupport = true;
    loader.grub.gfxmodeEfi = "1920x1080";
    plymouth.enable = true;
  };

  console.earlySetup = true;

  networking = {
    hostName = "silver";
    hostId = "ef44cfcc";
    firewall.enable = false;
    useDHCP = false;
    bridges.br0.interfaces = [ "enp6s0" ];
    interfaces.br0.useDHCP = true;
  };

  hardware.bluetooth.enable = true;

  environment.systemPackages = with pkgs; [
    unstable.zenstates
    looking-glass-client
    scream-receivers
  ];

  systemd.tmpfiles.rules = [
    "f /dev/shm/looking-glass 0660 kcrook qemu-libvirtd -"
    "f /dev/shm/scream        0660 kcrook qemu-libvirtd -"
  ];

  fileSystems."/srv/vm/installer" = {
    device = "192.168.1.54:/srv/vm/installer";
    fsType = "nfs";
    options = ["x-systemd.automount" "noauto"];
  };

  fileSystems."/srv/library" = {
    device = "192.168.1.54:/srv/library";
    fsType = "nfs";
    options = ["x-systemd.automount" "noauto"];
  };

  system.stateVersion = "20.03";

}
