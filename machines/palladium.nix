{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./roles/grub.nix
    ./roles/common.nix
    ./roles/desktop.nix
    ./roles/games.nix
  ];

  boot = {
    plymouth.enable = true;
    initrd.availableKernelModules = [ "i915" ];
    loader.grub.gfxmodeEfi = "1920x1080";
    kernelParams = [ "vga=current" "quiet" "splash" "loglevel=3" "rd.udev.log_priority=3" "vt.global_cursor_default=0" ];
  };

  console.earlySetup = true;

  networking = {
    hostName = "palladium";
    firewall.enable = false;
    networkmanager.enable = true;
  };

  hardware.bluetooth.enable = true;

  system.stateVersion = "20.03";

}
