{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./roles/grub.nix
    ./roles/common.nix
  ];

  boot = {
    kernelParams = [ "video=1920x1080@60" ];
    kernelPackages = pkgs.linuxPackages_hardened;
    supportedFilesystems = [ "zfs" ];
    zfs.enableUnstable = true;
    zfs.extraPools = [ "zmirror" ];
    loader.grub.zfsSupport = true;
    initrd.secrets = {
      "/xenon-znvme.key" = "/xenon-znvme.key";
      "/xenon-zmirror.key" = "/xenon-zmirror.key";
    };
  };

  services.zfs.trim.enable = true;

  networking = {
    hostName = "xenon";
    hostId = "8425e349";
    firewall.enable = false;
    useDHCP = false;
    bridges.br0.interfaces = [ "enp3s0" ];
    interfaces.br0.useDHCP = true;
  };

  services.ntp.enable = true;

  virtualisation = {
    libvirtd = {
      enable = true;
      allowedBridges = [ "br0" ];
      qemuOvmf = true;
      qemuRunAsRoot = false;
      onBoot = "ignore";
      onShutdown = "shutdown";
    };
    lxd = {
      enable = true;
      recommendedSysctlSettings = true;
      zfsSupport = true;
    };
  };

  services.nfs.server = {
    enable = true;
    exports = "
      /srv               192.168.1.0/24(ro,sync,fsid=0,no_subtree_check)
      /srv/vm/installer  192.168.1.0/24(rw,sync,nohide,insecure,no_subtree_check)
      /srv/library       192.168.1.0/24(rw,sync,nohide,insecure,no_subtree_check)
    ";
  };

  services.samba = {
    enable = true;
    securityType = "user";
    syncPasswordsByPam = true;
    extraConfig = "
      server string = xenon
      netbois name = xenon
      workgroup = PERIODIC
      socket options = TCP_NODELAY IPTOS_LOWDELAY SO_KEEPALIVE
      hosts allow = 192.168.1. localhost
      hosts deny = 0.0.0.0/0
      security = user
      wins support = yes
      guest account = nobody
      map to guest = bad user
    ";
    shares = {
      Library = {
        path = "/srv/library";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "no";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force group" = "users";
      };
      Installer = {
        path = "/srv/vm/installer";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "no";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force group" = "users";
      };
    };
  };

  system.stateVersion = "19.09";

}

