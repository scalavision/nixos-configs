{ config, pkgs, ... }:

{

  environment.systemPackages = with pkgs; [
    kodi
    python27Packages.pip
  ];

  nixpkgs.config = {
    allowUnfree = true;
    packageOverrides = pkgs: with pkgs; {
      kodi = pkgs.kodi.override {
        plugins = [ kodiPlugins.inputstream-adaptive ];
      };
    };
  };

}
