{ config, pkgs, ... }:

{

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    retroarch
    steam-run-native xboxdrv

    crawlTiles
    nethack
    brogue
    ivan
    cataclysm-dda
    endless-sky
  ];

  programs.steam.enable = true;

  hardware.pulseaudio.enable = true;

}
