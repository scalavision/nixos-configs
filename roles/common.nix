{ config, pkgs, ... }:

{

  nixpkgs.config.allowUnfree = true;

  system.copySystemConfiguration = true;

  boot = {
    cleanTmpDir = true;
    blacklistedKernelModules = [ "snd_pcsp" ];
  };

  networking.domain = "lem0n.zapto.org";

  i18n.defaultLocale = "en_CA.UTF-8";

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  time.timeZone = "America/Regina";

  environment.systemPackages = with pkgs; [
    usbutils pciutils nfs-utils cifs-utils gptfdisk psmisc file
    curl wget rsync unzip
    htop git python
    nox
    ( import ./vim.nix )
  ];

  programs = {
    bash.enableCompletion = true;
    tmux.enable = true;
    vim.defaultEditor = true;
    zsh = {
      enable = true;
      ohMyZsh = {
        enable = true;
        plugins = [ "colored-man-pages" "common-aliases" "extract" "git" "nix" "python" "sudo" "tmux" ];
        customPkgs = with pkgs; [
          zsh-powerlevel10k
          nix-zsh-completions
          powerline-fonts
        ];
      };
      promptInit = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      autosuggestions.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
    };
  };

  hardware = {
    enableRedistributableFirmware = true;
    cpu.amd.updateMicrocode = true;
    cpu.intel.updateMicrocode = true;
  };

  security.sudo.enable = true;

  services.openssh = {
    enable = true;
  };

  services.haveged.enable = true;

  users.users.kcrook = {
    isNormalUser = true;
    uid = 1000;
    description = "Kevin Crook";
    extraGroups = [ "audio" "cdrom" "disk" "libvirtd" "lxd" "networkmanager" "wheel" ];
    initialPassword = "changeme";
    shell = "${pkgs.zsh}/bin/zsh";
  };

}
