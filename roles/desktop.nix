{ config, pkgs, ... }:

{

  imports = [
    ./pidgin.nix
    ./kodi.nix
  ];

  environment.systemPackages = with pkgs; [
    ntfs3g
    lsd
    ark unar unrar p7zip rpm debootstrap vifm dpkg
    calibre deluge firefox filelight kate kcalc
    kdeApplications.krdc krename-qt5 kdeApplications.okular
    kdeconnect cool-retro-term
    virtmanager
    vlc spotify
    arc-theme arc-kde-theme arc-icon-theme libsForQt512.qtstyleplugin-kvantum
    libreoffice
    kodi
    xscreensaver caffeine-ng
    youtube-dl
    neovim-qt
  ];

  nixpkgs.config.firefox.enablePlasmaBrowserIntegration = true;

  fonts.fonts = with pkgs; [
    liberation_ttf
    fira
    fira-code
    fira-code-symbols
    inconsolata-nerdfont
    powerline-fonts
    proggyfonts
  ];

  programs.adb.enable = true;

  services.printing = {
    enable = true;
    drivers = [ pkgs.hplip ];
  };

  virtualisation.lxd = {
    enable = true;
    recommendedSysctlSettings = true;
  };

  virtualisation.libvirtd = {
    enable = true;
    allowedBridges = [ "br0" ];
    qemuOvmf = true;
    qemuRunAsRoot = false;
    onBoot = "ignore";
    onShutdown = "shutdown";
  };

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services.xserver = {
    enable = true;
    layout = "us";
    libinput.enable = true;
    displayManager.autoLogin.enable = true;
    displayManager.autoLogin.user = "kcrook";
    desktopManager.plasma5.enable = true;
    displayManager.sddm.enable = true;
  };

  users.users.kcrook.extraGroups = [ "adbusers" ];

}

