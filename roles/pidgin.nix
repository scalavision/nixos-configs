{ config, pkgs, ... }:

{

  environment.systemPackages = with pkgs; [
    pidgin-with-plugins
  ];

  nixpkgs.config = {
    allowUnfree = true;
    packageOverrides = pkgs: with pkgs; {
      pidgin-with-plugins = pkgs.pidgin-with-plugins.override {
        plugins = [ purple-facebook purple-discord purple-hangouts telegram-purple pidgin-latex pidgin-window-merge ];
      };
    };
  };

}
